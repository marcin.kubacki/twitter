## Installation
Application is running in docker using docker-compose. 
To install all dependencies needed for running application execute `install.sh` 
Command will run docker container and then run composer install command inside the php container

After installation set configuration in `application/.env` file. 
Example config file: `application/.env.exampe`

## Run application

To run application after installation execute `docker compose up`. 
Application is running on port 9001.

Example usage:

1. `curl localhost:9001` will return `Try /hello/:name`.

2. `curl localhost:9001/hello/AndrzejDuda` will return `Hello AndrzejDuda`.

3. `curl localhost:9001/histogram/AndrzejDuda` will return twitter user histogram.

4. Run tests
    - login to container `docker-compose run --rm php bash` 
    - run `vendor/bin/phpunit`.