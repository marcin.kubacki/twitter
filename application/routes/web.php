<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', 'Controller@main');
$router->get('/hello/{name}', 'Controller@hello');
$router->get('/histogram/{name}', 'Controller@histogram');
