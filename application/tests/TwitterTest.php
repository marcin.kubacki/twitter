<?php

class TwitterTest extends TestCase
{

    private $twitterClientFactory;

    /** @var \PHPUnit\Framework\MockObject\MockObject */
    private $twitterClient;

    public function setUp(): void
    {
        parent::setUp();
        $this->twitterClientFactory = $this->createMock(\App\Clients\TwitterClientFactory::class);
        $this->twitterClient = $this->createMock(\App\Clients\TwitterClient::class);
        $this->twitterClientFactory->method('getTwitterClient')->willReturn($this->twitterClient);
        $this->app->instance(\App\Clients\TwitterClientFactory::class, $this->twitterClientFactory);
    }

    public function testMainPage()
    {
        $this->get('/');

        $this->assertEquals('Try /hello/:name', $this->response->getContent());
    }

    public function testHelloPage()
    {
        $this->get('/hello/AndrzejDuda');

        $this->assertEquals('Hello AndrzejDuda', $this->response->getContent());
    }

    public function testEmptyHistogram()
    {
        $this->twitterClient->method('getUserTweets')->willReturn([]);
        $this->get('/histogram/AndrzejDuda');
        $this->assertEquals([], \json_decode($this->response->getContent(), true));
    }

    public function testUserHistogram()
    {
        $tweetsPart1 = [
            [
                'created_at' => 'Thu Apr 06 23:28:43 +0000 2019',
                'id' => 9
            ],
            [
                'created_at' => 'Thu Apr 05 0:28:43 +0000 2019',
                'id' => 8
            ],
            [
                'created_at' => 'Thu Apr 05 0:29:43 +0000 2019',
                'id' => 7
            ]
        ];
        $tweetsPart2 = [
            [
                'created_at' => 'Thu Apr 06 5:29:43 +0000 2017',
                'id' => 3
            ]
        ];

        $this->twitterClient->method('getUserTweets')->willReturnOnConsecutiveCalls($tweetsPart1, $tweetsPart2, []);
        $response = $this->get('/histogram/AndrzejDuda');
        $expectedValue = [
            0 => 2,
            5 => 1,
            23 => 1
        ];
        $this->assertEquals($expectedValue, \json_decode($this->response->getContent(), true));
    }
}
