<?php

namespace App\Http\Controllers;

use App\Services\TwitterService;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    /** @var TwitterService */
    private $twitterService;

    public function __construct(TwitterService $twitterService)
    {
        $this->twitterService = $twitterService;
    }

    public function main(): string
    {
        return 'Try /hello/:name';
    }

    public function hello(string $name): string
    {
        return 'Hello ' . $name;
    }

    public function histogram(string $name)
    {
        return new JsonResponse($this->twitterService->getHistogramByUserName($name));
    }

}
