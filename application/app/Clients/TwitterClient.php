<?php

namespace App\Clients;

use GuzzleHttp\Client;

class TwitterClient
{

    private const TWEETS_LIMIT = 1000;
    private const TWITTER_FIELD_SCREEN_NAME = 'screen_name';
    private const TWITTER_FIELD_TRIM_USER = 'trim_user';
    private const TWITTER_FIELD_COUNT = 'count';
    private const TWITTER_FIELD_MAX_ID = 'max_id';

    /** @var Client */
    private $httpClient;

    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getUserTweets(string $user, ?string $maxId = null): array
    {
        $query = [
            self::TWITTER_FIELD_SCREEN_NAME => $user,
            self::TWITTER_FIELD_TRIM_USER => true,
            self::TWITTER_FIELD_COUNT => self::TWEETS_LIMIT
        ];
        if ($maxId) {
            $query[self::TWITTER_FIELD_MAX_ID] = $maxId;
        }
        $response = $this->httpClient->get('statuses/user_timeline.json', ['query' => $query]);
        return \json_decode((string) $response->getBody(), true);
    }
}
