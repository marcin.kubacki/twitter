<?php

namespace App\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use GuzzleHttp\HandlerStack;

class TwitterClientFactory
{

    private const TWITTER_BASE_URL = 'https://api.twitter.com/1.1/';

    public function getTwitterClient(): TwitterClient
    {
        $stack = HandlerStack::create();

        $middleware = new Oauth1([
            'consumer_key'    => env('TWITTER_API_KEY'),
            'consumer_secret' => env('TWITTER_API_SECRET_KEY'),
            'token'           => env('TWITTER_ACCESS_TOKEN'),
            'token_secret'    => env('TWITTER_ACCESS_TOKEN_SECRET')
        ]);
        $stack->push($middleware);

        $httpClient = new Client([
            'base_uri' => self::TWITTER_BASE_URL,
            'timeout' => env('TWITTER_TIMEOUT'),
            'handler' => $stack,
            'auth' => 'oauth'
        ]);
        return new TwitterClient($httpClient);
    }
}
