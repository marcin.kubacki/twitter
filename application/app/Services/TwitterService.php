<?php

namespace App\Services;

use App\Clients\TwitterClient;
use App\Clients\TwitterClientFactory;
use Carbon\Carbon;

class TwitterService
{

    /** @var TwitterClient */
    private $twitterClient;

    public function __construct(TwitterClientFactory $twitterClientFactory)
    {
        $this->twitterClient = $twitterClientFactory->getTwitterClient();
    }

    public function getHistogramByUserName(string $user): array
    {
        $result = [];
        $maxId = null;
        $getTweets = true;
        while ($getTweets) {
            $tweets = $this->twitterClient->getUserTweets($user, $maxId);
            foreach ($tweets as $tweet) {
                $date = new Carbon($tweet['created_at']);
                $result[$date->hour] = isset($result[$date->hour]) ? $result[$date->hour] + 1 : 1;
            }
            $getTweets = !!\count($tweets);
            if ($getTweets) {
                $last = end($tweets);
                $maxId = $last['id'] - 1;
            }
        }
        return $result;
    }
}
